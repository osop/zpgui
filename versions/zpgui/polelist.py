from PyQt4.QtCore import *
from PyQt4.QtGui  import *

import sys


class FormWidget(QWidget):

	def __init__(self, parent=None):        
	        super(FormWidget, self).__init__(parent)
		self.editor = WidgetA(self)
		self.button = QPushButton("Add poles",self)
	        self.layout = QVBoxLayout(self)
		self.example = QLabel('The input format should be included all parameters and no space.\nExample:   0+0j\n                     2e5+8j')
		self.layout.addWidget(self.editor)
		self.layout.addWidget(self.example)
		self.layout.addWidget(self.button)
	 	self.connect(self.button,SIGNAL("clicked()"),self.editor.saveFile)
		self.button.clicked.connect(QApplication.instance().quit)
		
	def get(self):
		readfile = []
		temp=[]
		for line in open('polelist.txt','r').readlines():
			if (line.strip()==''):
				pass
			else:
				readfile.append(line.strip())
		return readfile

class WidgetA(QTextEdit):
	def __init__(self, parent):
		super(WidgetA, self).__init__(parent)
		self.f= open('polelist.txt','r')
        	self.setPlainText('')
		self.f.close()
	def saveFile(self):
		if (self.toPlainText()==''):
			pass
		else:
			self.f = open('polelist.txt', 'a')
			self.filedata = self.toPlainText()
			self.f.write(self.filedata+'\n')
			self.f.close()
