======================================================================
HOW TO INSTALL PYTHON-MATPLOTLIB v1.1.1rc1 & PYTHON SYMPY
sudo apt-get update
sudo apt-get install python-matplotlib
sudo apt-get install python-sympy

======================================================================
HOW TO INSTALL PYTHON-NUMPY v1.6.2
# Step 1:
	wget http://downloads.sourceforge.net/project/numpy/NumPy/1.6.2/numpy-1.6.2.tar.gz

# Step 2:
	tar -xvf numpy-1.6.2.tar.gz && cd numpy-1.6.2

# Step 3:
	sudo apt-get install libatlas-base-dev libatlas3gf-base

# Step 4:
	sudo apt-get install python-dev

# Step 5:
	sudo python setup.py install

***NOTE: Please remove the old version of numpy before install.
======================================================================
HOW TO INSTALL PYTHON-SCIPY

#Step 1: Download Scipy v0.12
	 http://sourceforge.net/projects/scipy/
# Step 2: Extract
	tar -xvf scipy-0.12.0.tar.gz
	cd scipy-0.12.0
# Step 3:
	sudo apt-get install gfortran g++

# Step 4:
	sudo python setup.py install

***NOTE: Please remove the old version of scipy before install.