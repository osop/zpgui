## Ported by OSOP, Volcan, Panama June, 2012: http://www.osop.com.pa
##
##
## OSOP Programmer: Stacey Anne Rieck (stace.rieck@gmail.com)
## OSOP Project Director: Branden Carl Christensen (branden.christensen@osop.com.pa)
##
##
## Original MATLAB scripts: 
## plot_z_surface.m by David Dorran (david.dorran@dit.ie) [2011] 
## zpgui.m Author: Tom Krauss (9/1/98); Adapted by: David Dorran [2011]
## For original MATLAB scripts see: http://dadorran.wordpress.com/2012/04/07/zpgui/
##

from pylab import *
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import numpy
from mpl_toolkits.mplot3d.axes3d import Axes3D
from  matplotlib import patches
from matplotlib.figure import Figure
from matplotlib import rcParams
from matplotlib import colors
from matplotlib.widgets import CheckButtons


## EDITABLE VALUES----------------------------- 
## Surface resolution between 0.02 and 0.1
## 0.02 is a very high resolution plot (very slow)
## 0.1 is a very low resolution plot (fast)
## Default value is 0.05
## DISCLAIMER: Choosing numbers outside of this range could result in unusual/unexpected behavior. Use at your own risk.
surface_resolution = 0.1
##---------------------------------------------

plt.ioff()
z = [0,0]
p = [0,0]
zh = [0,0]
ph = [0,0]
ax = [0,0]
plabels = [0,0]
zlabels = [0,0]
thisline = None
polecount = 0;
zerocount = 0;
surface_toggle = 1;
disable_3d_global = True;
version = 1.03
highres = False

fig2_closed = False;
warnings.simplefilter("ignore", ComplexWarning)

def recompute(self=None, event=None):
	global axfig2,disable_3d_global,freq,fig,fig2
	global z,p,freq,surf
	
	plotfilterchar();

	if (disable_3d_global == False):
		plot_z_surface(p,z,axfig2)
		fig2.canvas.draw();
		
	fig.canvas.draw();
	
def recompute_only_fig1(self=None, event=None):
	global ax,disable_3d_global,freq,fig,fig2
	global z,p,freq,surf
	
	plotfilterchar();
		
	fig.canvas.draw();	

def zplane(ax):

	uc = patches.Circle((0,0), radius=1, fill=False,color='black', ls='dashed')
	ax.add_patch(uc)


    # set the ticks
	r = 1.5; plt.axis('scaled'); 
	plt.axis([-r, r, -r, r])
	ticks = [-1, -.5, 0, .5, 1]; 
	plt.xticks(ticks); plt.yticks(ticks)
	plt.xlabel('Real Part')
	plt.ylabel('Imaginary Part')
	plt.grid(b=True, which='major', axis='both');

	return ax
	
def plot_z_surface(pole_positions_orig, zero_positions_orig,ax, CameraPos=None,CameraUpVec=None,surface_display_opts=None):
	
	global surf,surface_resolution,z_grid,X,Y,fig2
	surface_limit = 1.7;
	min_val = -surface_limit;
	max_val = surface_limit;

	ax = fig2.add_subplot(111,projection='3d')

	X = numpy.arange(min_val,max_val,surface_resolution)
	Y = numpy.arange(min_val,max_val,surface_resolution)
	X, Y = numpy.meshgrid(X, Y)

	z_grid = X + Y*1j;
	z_surface = z_grid*0;
	
	pole_positions = numpy.round(pole_positions_orig,1) + surface_resolution/2+(surface_resolution/2)*1j;
	zero_positions = numpy.round(zero_positions_orig,1) + surface_resolution/2 +(surface_resolution/2)*1j;

	for k in range(0, len(zero_positions)):
		if (zero_positions_orig[k].imag == 0):
			z_surface = z_surface + 20*log10((z_grid - zero_positions[k]));		
		else:
			z_surface = z_surface + 20*log10((z_grid - zero_positions[k].real - zero_positions[k].imag*1j));
			z_surface = z_surface + 20*log10((z_grid - zero_positions[k].real + zero_positions[k].imag*1j));
		
	for k in range(0, len(pole_positions)):
		if (pole_positions_orig[k].imag == 0):
			z_surface = z_surface - 20*log10((z_grid - pole_positions[k]));		
		else:
			z_surface = z_surface - 20*log10((z_grid - pole_positions[k].real - pole_positions[k].imag*1j));
			z_surface = z_surface - 20*log10((z_grid - pole_positions[k].real + pole_positions[k].imag*1j));	

	cmap = cm.jet
	#cmap.set_bad(color='w')
	lev = numpy.arange(-30,30,1);
	norml = colors.BoundaryNorm(lev, 256)

	Zm = ma.masked_where((abs(z_grid) < 1.02) & (abs(z_grid) > 0.98), (z_surface))
	z_surface[where(ma.getmask(Zm)==True)] = numpy.nan
	
	if (surface_toggle == 1):
		surf = ax.plot_surface(X, Y, z_surface, rstride=1, cstride=1, cmap=cm.jet,linewidth=0, antialiased=True, norm = norml)
		

	else:
		surf = ax.plot_wireframe(X, Y, z_surface, rstride=1, cstride=1)

	
	ticks = [-1, 1]; 
	z_ticks = [-30,-20,-10,0,10,20,30]; 
	ax.set_xticks(ticks);
	ax.set_yticks(ticks);	
	ax.set_zticks(z_ticks);
	ax.set_zlim(-30, 30);
	ax.set_xlabel('Re')
	ax.set_ylabel('Im')
	ax.set_zlabel('Mag(db)',ha='left')
	plt.setp(ax.get_zticklabels(), fontsize=7)
	plt.setp(ax.get_xticklabels(), fontsize=7)	
	plt.setp(ax.get_yticklabels(), fontsize=7)
		
	ax.grid(b=None);

	
def plotfilterchar():
	global z,p,freq,ax
	
	b = [1]
	a = [1]
	
	axes(ax[1])
	plt.cla();

	for i in numpy.arange(0,len(z)):
		if (z[i].imag == 0):
			b = numpy.convolve(b,[1, -1*z[i]])
		else:
			b = numpy.convolve(b,[1, -2*(z[i].real),numpy.abs(z[i])*numpy.abs(z[i])])		

	for i in numpy.arange(0,len(p)):
		if (p[i].imag == 0):
			a = numpy.convolve(a,[1, -1*p[i]])
		else:
			a = numpy.convolve(a,[1, -2*(p[i].real),numpy.abs(p[i])*numpy.abs(p[i])])	
			
	Nfft = 256;

	X = arange(0,0.5,0.001953125*2)
	Y = numpy.fft.fft(b,Nfft)/numpy.fft.fft(a,Nfft)

	freq = ax[1].plot(X,20*log10((numpy.abs((Y[0:128])))));
	ax[1].set_ylabel('Magnitude (db) ')
	ax[1].set_xlabel('Normalised Frequency')		
	ax[1].set_ylim(-30, 30);
			
def zpgui(arg1=None,arg2=None,arg3=None):

	global fig,fig2,sslider

	def onpick(event):
		global thisline
		if isinstance(event.artist, Line2D):
			thisline = event.artist

	def onmove(event):
		global thisline
		if event.inaxes != ax[0]: return
		if thisline is None: return	
		axes(ax[0])
		objectlabel = thisline.get_label()
		objecttype = objectlabel[0]
		objectvalue = int(objectlabel[1]);
		if (objectvalue % 2):
			oppositevalue = objectvalue-1;
			value = objectvalue/2;
		else:
			oppositevalue = objectvalue+1;
			value = (objectvalue+1)/2;

			
		if (objecttype == 'p'):
			plt.setp(ph[objectvalue], xdata=event.xdata,ydata=event.ydata)
			plt.setp(ph[oppositevalue], xdata=event.xdata,ydata=event.ydata*-1)
			p[value] = event.xdata+event.ydata*1j
			
		else:
			plt.setp(zh[objectvalue], xdata=event.xdata,ydata=event.ydata)
			plt.setp(zh[oppositevalue], xdata=event.xdata,ydata=event.ydata*-1)	
			z[value] = event.xdata+event.ydata*1j		
			
		text = 'ZPGUI Version: ' + repr(version) + '      Selected Position: ' + repr(round(event.xdata,5)) + ' ' + repr(round(event.ydata,5)) + 'j';
			
		axtext.cla();
		axtext.text(1,-1.5,text,visible='True');

		recompute_only_fig1();

	def onrelease(event):
		global thisline
		thisline = None
		if event.inaxes != ax[0]: return		
		recompute();
	
	def onfig1close(event):
		plt.close()
		
	def onfig2close(event):
		global fig2_closed,disable_3d_global
		fig2_closed = True;
		disable_3d_global = True;
		axfig2 = None
		
	def open3dwindow(event):
		global fig2,axfig2,disable_3d_global,z,p
		fig2 = plt.figure(figsize=plt.figaspect(0.75))
		fig2.canvas.mpl_connect('close_event', onfig2close)
		
		axfig2 = fig2.add_subplot(1, 1, 1,projection='3d')


		disable_3d_global = False;
		plot_z_surface(p,z,axfig2)
		show()
		
	def toggle_surface_display(event):
		global surface_toggle
		if (surface_toggle == 0):
			surface_toggle = 1;
		else:
			surface_toggle = 0;
		recompute();
		
	def addpole(event):
		global ph, ax, polecount, p
		
		plt.axes(ax[0])
		
		newlabel = 'p%d' %(polecount)
		newhandler = plt.plot(0.5, 0, 'x', ms=10)
		plt.setp(newhandler, markersize=12.0, markeredgewidth=1.0,markeredgecolor='b',picker=5,label=newlabel)
		ph.append(newhandler);
		p.append(0.5+0j)
		polecount=polecount+1
		
		newlabel = 'p%d' %(polecount)
		new_handler = plt.plot(0.5, 0, 'x', ms=10)
		plt.setp(new_handler, markersize=12.0, markeredgewidth=1.0,markeredgecolor='b',picker=5,label=newlabel)
		ph.append(new_handler);
		polecount=polecount+1		
		recompute();
		
		
		
	def addzero(event):
		global zh
		global ax
		global zerocount
		global z
		plt.axes(ax[0])
		
		newlabel = 'z%d' %(zerocount)
		new_handler = plt.plot(0.5, 0, 'wo', ms=10)
		plt.setp(new_handler, markersize=10.0, markeredgewidth=1.0,markeredgecolor='b',picker=5,label=newlabel)
		zh.append(new_handler);
		z.append(0.5+0j)
		zerocount=zerocount+1
		
		newlabel = 'z%d' %(zerocount)
		new_handler = plt.plot(0.5, 0, 'wo', ms=10)
		plt.setp(new_handler, markersize=10.0, markeredgewidth=1.0,markeredgecolor='b',picker=5,label=newlabel)
		zh.append(new_handler);
		zerocount=zerocount+1
		recompute();
		
	def removepole(event=None):
		global polecount,p,ph

		if (len(p) != len(ph)/2):
			print 'ERROR!'
		if (len(ph)!= polecount):
			print 'ERROR!'
			
		if len(p) > 0:
			plt.setp(ph[polecount-1], picker=None,visible=False)
			plt.setp(ph[polecount-2], picker=None,visible=False)
			p.pop();
			polecount = polecount-2;
			ph.pop();
			ph.pop();
			
		recompute();

	def removezero(event):
		global zerocount,z,zh
		
		if (len(z) != len(zh)/2):
			print 'ERROR!'
		if (len(zh)!= zerocount):
			print 'ERROR!'
			
		if len(z) > 0:
			plt.setp(zh[zerocount-1], picker=None,visible=False)
			plt.setp(zh[zerocount-2], picker=None,visible=False)
			z.pop();
			zerocount = zerocount-2;
			zh.pop();
			zh.pop();
		recompute();
		
	f = open('ZPGUIDebug', 'w')
	
	global z,p,zh,ph,ax,press,polecount,zerocount,disable_3d_global

	if (arg1 is None) and (arg2 is None) and (arg3 is None):
		f.write('No arguments received, using default peramaters...\n')
		action = 'init';
		z = [0.29-0.41j];
		p = [-0.6+0.73j];
	elif (arg1 is not None) and (arg2 is not None):
		f.write('Two arguments received, using them for the pole and zero initial values...\n')
		action = 'init';
		p = arg1;
		z = arg2;
	else:
		action = arg1;

	
	surface_display_opts = 0;
	
	f.write('Plotting Figure...\n')
	fig = plt.figure(figsize=plt.figaspect(0.75))
	ax[0] = fig.add_subplot(2, 2, 1)
	
	axtext = axes([0.00, 1.05, 0.16, 0.075],visible='False')
	
	text = 'ZPGUI Version: ' + repr(version);

	axtext.cla();
	axtext.text(1,-1.5,text,visible='True');
		
	f.write('Adding Buttons...\n')
	axaddpoles = axes([0.54, 0.83, 0.16, 0.075])
	baddpoles = Button(axaddpoles, 'Add Poles')
	baddpoles.on_clicked(addpole)

	axaddzeros = axes([0.71, 0.83, 0.16, 0.075])
	baddzeros = Button(axaddzeros, 'Add Zeros')
	baddzeros.on_clicked(addzero)

	axremovepoles = axes([0.54, 0.73, 0.16, 0.075])
	bremovepoles = Button(axremovepoles, 'Remove Poles')
	bremovepoles.on_clicked(removepole)
		
	axremovezeros = axes([0.71, 0.73, 0.16, 0.075])
	bremovezeros = Button(axremovezeros, 'Remove Zeros')
	bremovezeros.on_clicked(removezero)

	axtoggle = axes([0.58, 0.63, 0.25, 0.075])
	btoggle = Button(axtoggle, 'Toggle surface display')
	btoggle.on_clicked(toggle_surface_display)

	ax3d = axes([0.58, 0.53, 0.25, 0.075])
	b3d = Button(ax3d, 'Open 3D window')
	b3d.on_clicked(open3dwindow)
	
	axes(ax[0])
	
	f.write('Plotting z-plane...\n')
	zplane(ax[0]);
	
	for i in arange(0,len(z),2):
	
		newlabel = 'z%d' %(zerocount)
		zh[i], = plt.plot(z[i].real, z[i].imag, 'wo', ms=10)
		plt.setp(zh[i], markersize=10.0, markeredgewidth=1.0,markeredgecolor='b',picker=5,label= newlabel)
		zerocount=zerocount+1
		
		newlabel = 'z%d' %(zerocount)
		zh[i+1], = plt.plot(z[i].real, z[i].imag*-1, 'wo', ms=10)
		plt.setp(zh[i+1], markersize=10.0, markeredgewidth=1.0,markeredgecolor='b',picker=5,label= newlabel)
		zerocount=zerocount+1
		
	for i in range(0,len(p),2):
	
		newlabel = 'p%d' %(polecount)
		ph[i], = plt.plot(p[i].real, p[i].imag, 'x', ms=10)
		plt.setp(ph[i], markersize=10.0, markeredgewidth=1.0,markeredgecolor='b',picker=5,label= newlabel)
		polecount=polecount+1
		
		newlabel = 'p%d' %(polecount)
		ph[i+1], = plt.plot(p[i].real, p[i].imag*-1, 'x', ms=10)
		plt.setp(ph[i+1], markersize=10.0, markeredgewidth=1.0,markeredgecolor='b',picker=5,label= newlabel)
		polecount=polecount+1

		
	ax[1] = fig.add_subplot(2, 1, 2)
	
	f.write('Plotting Characteristic...\n')
	plotfilterchar()

		

	fig.canvas.mpl_connect('pick_event', onpick)
	fig.canvas.mpl_connect('motion_notify_event', onmove)
	fig.canvas.mpl_connect('button_release_event', onrelease)
	fig.canvas.mpl_connect('close_event', onfig1close)
	#fig2.canvas.mpl_connect('close_event', onclose)
	
	f.write('Plot Complete.\n')	
	show()
	

