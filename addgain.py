from PyQt4.QtCore import *
from PyQt4.QtGui  import *

import sys


class FormWidget(QWidget):

	def __init__(self, parent=None):        
	        super(FormWidget, self).__init__(parent)
		self.editor = WidgetA(self)
		self.button = QPushButton("New gain",self)
	        self.layout = QVBoxLayout(self)
		self.example = QLabel('Example: 2e7')
		self.layout.addWidget(self.editor)
		self.layout.addWidget(self.example)
		self.layout.addWidget(self.button)
	 	self.connect(self.button,SIGNAL("clicked()"),self.editor.saveFile)
		self.button.clicked.connect(QApplication.instance().quit)

class WidgetA(QLineEdit):
	def __init__(self, parent=None):
		super(WidgetA, self).__init__(parent)
	def saveFile(self):
		if (self.text()==''):
			pass
		else:
			self.f = open('file.txt', 'w')
			self.filedata = self.text()
			self.f.write(self.filedata+'\n')
			self.f.close()
